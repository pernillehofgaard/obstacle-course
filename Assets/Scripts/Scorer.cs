using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scorer : MonoBehaviour
{
    int score = 100;
    int coinsCollected = 0;
    
    private void Start(){
        Debug.Log($"Score: {score}");
    }
    
    private void OnCollisionEnter(Collision other) 
    {
        if(other.gameObject.tag == "obstacle"){
            score -= 10;
            Debug.Log($"Score: {score}");
        }

        if(other.gameObject.tag == "Coin"){
            coinsCollected++;        
            score += 10;    
            Debug.Log($"Coins collected: {coinsCollected}");
            Debug.Log($"Score: {score}");         
        }                
    }
}
