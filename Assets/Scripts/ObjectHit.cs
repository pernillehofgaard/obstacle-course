using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectHit : MonoBehaviour
{
    float timer;

    private void Start() {
        ;    
    }

    private void Update() {
        timer = Time.time;
    }
    private void OnCollisionEnter(Collision other) //other - what collided with the object
    {
        if(other.gameObject.tag == "Player")
        {    

            //remove coin
            if(gameObject.tag == "Coin"){
                GetComponent<MeshRenderer>().enabled = false;
                GetComponent<SphereCollider>().enabled = false;
            }

            if(gameObject.tag == "obstacle"){

            
                GetComponent<MeshRenderer>().material.color = Color.red;            
                gameObject.tag = "IsHit";
            }
            

            if(gameObject.tag == "Finish"){
                gameObject.tag = "Player";
                Debug.Log($"Finished in: {timer} seconds");      
            }
        }
    }
}
