using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Put on Player gameObject
public class Mover : MonoBehaviour
{        
    // Start is called before the first frame update
    [SerializeField]float moveSpeed = 10f;

    void Start()
    {        
        //transform.Translate(1,0,0); //moves object when game starts
        PrintInsturctions();
    }

    // Update is called once per frame
    void Update()
    {
        //Moves object with x-axis per fps
        //transform.Translate(1,0,0);
        //transform.Translate(0.1f,0,0); decimal number needs f - float
        //transform.Translate(xValue,yValue,zValue);
        MovePlayer();
        
    }

    void MovePlayer(){
        float xValue = Input.GetAxis("Horizontal") * Time.deltaTime * moveSpeed; //Time.deltaTime makes the game frame independent
        float zValue = Input.GetAxis("Vertical") * Time.deltaTime * moveSpeed;
        transform.Translate(xValue, 0, zValue);
    }

    void PrintInsturctions(){
        Debug.Log("Welcome!");
        Debug.Log("wasd to move");
        Debug.Log("Or arrow keys to move");
    }    
}
